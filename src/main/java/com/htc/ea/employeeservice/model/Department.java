package com.htc.ea.employeeservice.model;


import java.util.ArrayList;
import java.util.List;

public class Department{
	private Long id;
	private Long idOrganization;
	private String name;
	
	private List<Employee> employees = new ArrayList<>();

	public Department() {
		
	}

	public Department(Long idOrganization, String name) {
		super();
		this.idOrganization = idOrganization;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getIdOrganization() {
		return idOrganization;
	}

	public void setIdOrganization(Long idOrganization) {
		this.idOrganization = idOrganization;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Department [");
		if (id != null) {
			builder.append("id=");
			builder.append(id);
			builder.append(", ");
		}
		if (idOrganization != null) {
			builder.append("idOrganization=");
			builder.append(idOrganization);
			builder.append(", ");
		}
		if (name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if (employees != null) {
			builder.append("employees=");
			builder.append(employees);
		}
		builder.append("]");
		return builder.toString();
	}

	
	
	

}
