package com.htc.ea.employeeservice.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.htc.ea.employeeservice.client.DeparmentApiClient;
import com.htc.ea.employeeservice.model.Department;
import com.htc.ea.employeeservice.model.Employee;
import com.htc.ea.employeeservice.repository.EmployeeRepository;
import com.htc.ea.employeeservice.util.AppConst;

@Service
public class EmployeeService {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private DeparmentApiClient apiClient;
	
	public ResponseEntity<Employee> save(Employee employee){
		//datos que iran en response entity
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		Employee result = null;
		try {
			//consumir microservicio de department
			ResponseEntity<Department> response = apiClient.findDepartmentById(employee.getIdDepartment());
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				//verifico contenido de la respuestas a la servicio de deparment
				if(response.getBody()!=null) {
					//establezco id de departamento buscado a la entidad employee
					employee.setIdOrganization(response.getBody().getIdOrganization());
					result = employeeRepository.saveAndFlush(employee);
					status = HttpStatus.OK;
					headers.add(AppConst.MSG_DESCRIPTION, "guardado correctamente");
					log.info("departamento guardado correctamente");
				}else {
					//si no existe el department entonces se manda un bad request
					//ademas de detalles del error
					status = HttpStatus.BAD_REQUEST;
					headers.add(AppConst.MSG_ERROR, "No existe departamento con id "+employee.getIdDepartment());
				}
			}else {
				//en caso que el estado no sea OK
				//se setea los headers de la consulta a esta respuesta
				headers.addAll(response.getHeaders());
				status = response.getStatusCode();
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar guardar departamento");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de employee
	 * busca un employee dado el id ingresado
	 * maneja posibles resultados de: encontrado, no encontrado y exceptions
	 * @param id empleado
	 * @return response entity con cabeceras de mensajes, null en el body si no se encontro
	 */
	public ResponseEntity<Employee> findById(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.OK;//por defecto ok, solo cambia si entra en error
		Employee result = null;
		try {
			//buscar el employee, esto se envuelve en Optional
			Optional<Employee> searched = employeeRepository.findById(id);
			//si el employee esta presente
			if(searched.isPresent()) {
				result = searched.get();
				headers.add(AppConst.MSG_DESCRIPTION, "encontrado correctamente");
				log.info("empleado encontrado");
			}else {
				headers.add(AppConst.MSG_DESCRIPTION, "No se encontro empleado con id "+id);
			}
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar empleado con id "+id);
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	/**
	 * funcion de busqueda de todos los employee
	 * evalua si la lista esta vacia y tambien si ocurre algun error lo maneja con try-catch
	 * @return response entity con cabeceras de mensajes, null en el body si ocurrio alguna exception mientras se buscaba
	 */
	public ResponseEntity<List<Employee>> findAll(){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		List<Employee> result = null;
		//try catch para manejar exceptions que se puedan producir
		try {
			result = employeeRepository.findAll();
			status = HttpStatus.OK;
			headers.add(AppConst.MSG_DESCRIPTION,"Empleados encontrados : "+result.size());
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar todos los empleados");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	
	/**
	 * funcion de busqueda de todos los empleados bajo el id de una organizacion
	 * maneja errores con try-catch
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si ocurrio alguna exception mientras se buscaba
	 */
	public ResponseEntity<List<Employee>> findAllByIdOrganization(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		List<Employee> result = null;
		try {
			result = employeeRepository.findAllByIdOrganization(id);
			status = HttpStatus.OK;
			headers.add(AppConst.MSG_DESCRIPTION,"Empleados encontrados : "+result.size());
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar empleados");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	
	/**
	 * funcion de busqueda de todos los empleados bajo el id de department
	 * maneja errores con try-catch
	 * @param id organization
	 * @return response entity con cabeceras de mensajes, null en el body si ocurrio alguna exception mientras se buscaba
	 */
	public ResponseEntity<List<Employee>> findAllByIdDepartment(Long id){
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status;
		List<Employee> result = null;
		try {
			result = employeeRepository.findAllByIdDepartment(id);
			status = HttpStatus.OK;
			headers.add(AppConst.MSG_DESCRIPTION,"Empleados encontrados : "+result.size());
		} catch (Exception e) {
			//si ocurre un error se establecen errores de servidor
			log.error(e.getMessage());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			headers.add(AppConst.MSG_ERROR, "Ocurrio un error al intentar buscar empleados");
		}
		return new ResponseEntity<>(result,headers,status);
	}
	
	

}
