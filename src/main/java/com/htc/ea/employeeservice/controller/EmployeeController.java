package com.htc.ea.employeeservice.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.validation.Valid;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.htc.ea.employeeservice.dto.EmployeeRequest;
import com.htc.ea.employeeservice.model.Employee;
import com.htc.ea.employeeservice.service.EmployeeService;

@RestController
public class EmployeeController {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Value("${server.port}")
	private int serverPort;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private Mapper mapper;
	
	@PostMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> save(@Valid @RequestBody EmployeeRequest request) {
		showIpAndPort("POST / "+request.toString());
		return employeeService.save(mapper.map(request, Employee.class));
	}
	
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Employee> findById(@PathVariable("id") Long id) {
		showIpAndPort("GET /"+String.valueOf(id));
		return employeeService.findById(id);
	}
	
	@GetMapping(value="/",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> findAll() {
		showIpAndPort("GET /");
		return employeeService.findAll();
	}
	
	@GetMapping(value="/departments/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> findAllByIdDepartment(@PathVariable("id") Long id) {
		showIpAndPort("GET /departments/"+String.valueOf(id));
		return employeeService.findAllByIdDepartment(id);
	}
	
	@GetMapping(value="/organizations/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Employee>> findAllByIdOrganization(@PathVariable("id") Long id) {
		showIpAndPort("GET /organizations/"+String.valueOf(id));
		return employeeService.findAllByIdOrganization(id);
	}
	
	
	@GetMapping(value="/test")
	public ResponseEntity<String> getTest() {
		return new ResponseEntity<>("Hola mundo",HttpStatus.OK);
	}
	
	/**
	 * muestra en logs informacion de la peticion
	 * @param inf informacion del request de la peticion
	 */
	private void showIpAndPort(String inf) {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("IP:PORT ");
			builder.append(InetAddress.getLocalHost().getHostAddress());
			builder.append(":");
			builder.append(serverPort);
			builder.append(" REQUEST ");
			builder.append(inf);
			log.info(builder.toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
}
