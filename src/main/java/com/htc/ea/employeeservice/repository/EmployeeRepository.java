package com.htc.ea.employeeservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.htc.ea.employeeservice.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	public List<Employee> findAllByIdDepartment(Long id);
	public List<Employee> findAllByIdOrganization(Long id);
}
