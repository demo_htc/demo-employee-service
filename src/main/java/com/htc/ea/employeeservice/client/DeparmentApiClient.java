package com.htc.ea.employeeservice.client;

import org.springframework.http.ResponseEntity;

import com.htc.ea.employeeservice.model.Department;

/**
 * Interfaz de definicion de metodos de consumo de API department
 * consulta a microservicio de departamento y obtiene diversa informacion
 * @author htc
 *
 */
public interface DeparmentApiClient {
	
	/**
	 * consulta al microservicio department
	 * retorna el departamento bajo el id asociado
	 * contiene una funcion fallback en el caso de que no se pueda conectar al microservicio
	 * @param id departamento
	 * @return response entity con headers y department en el body
	 */
	public ResponseEntity<Department> findDepartmentById(Long id);
}
