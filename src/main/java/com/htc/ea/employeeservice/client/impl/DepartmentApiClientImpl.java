package com.htc.ea.employeeservice.client.impl;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.htc.ea.employeeservice.client.DeparmentApiClient;
import com.htc.ea.employeeservice.config.RestTemplateConfig;
import com.htc.ea.employeeservice.model.Department;

@Component
public class DepartmentApiClientImpl implements DeparmentApiClient{
	
	@Value("${service.department}")
    private String baseUri;
	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	RestTemplateConfig restConfig;
	
	/**
	 * consumo mediante restTemplate
	 */
	@Override
	public ResponseEntity<Department> findDepartmentById(Long id) {
		HttpHeaders headers = new HttpHeaders();
		try {
			StringBuilder composedUri = new StringBuilder();
			composedUri.append(baseUri); 			//https:{ip}:{puerto}
			composedUri.append("/"); 	//uri resultante 
			composedUri.append(String.valueOf(id));	//parametro
			
			
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<Department> entity = new HttpEntity<>(headers);
					
			return restConfig.getRestTemplate().exchange(composedUri.toString(), HttpMethod.GET,entity, Department.class);
			
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e.toString());
			headers.add("Error", "No se pudo acceder al servicio");
			return new ResponseEntity<>(null,headers,HttpStatus.SERVICE_UNAVAILABLE);
		}
	}
}
